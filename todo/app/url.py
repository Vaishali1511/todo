from django.contrib import admin
from django.urls import path,include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [

    path('', views.home_page,name='home_page'),
    path('home_views/', views.home_views,name='home'),
    path('login/', views.login_views,name='login'),
    path('signup/', views.signup_views,name='signup'),
    path('add-todo/', views.add_todo, name='add_todo'),
    path('logout/', views.signout, name='add_todo'),
    path('delete-todo/<int:id>', views.delete_todo, name='delete_todo'),
    path('change-status/<int:id>/<str:status>' , views.change_todo, name='change_todo' ),

    path('reset_password/',
         auth_views.PasswordResetView.as_view(template_name="password_reset.html"),
         name="reset_password"),

    path('reset_password_sent/',
         auth_views.PasswordResetDoneView.as_view(template_name="password_reset_sent.html"),
         name="password_reset_done"),

    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="password_reset_form.html"),
         name="password_reset_confirm"),

    path('reset_password_complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name="password_reset_done.html"),
         name="password_reset_complete"),
]