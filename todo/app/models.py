from django.db import models
from django.contrib.auth.models import User

class TODO(models.Model):
    STATUS_CHOICES = [
    ('C', 'COMPLETED'),
    ('P', 'PENDING'),
]
    PRIORITY_CHOICES = [
        ('1', 'HIGH'),
        ('2', 'MEDIUM'),
        ('3', 'LOW'),
    ]
    title = models.CharField(max_length=50)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES)
    date = models.DateTimeField(auto_now_add=True)
    user  = models.ForeignKey(User  , on_delete= models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    priority = models.CharField(max_length=2 , choices=PRIORITY_CHOICES)